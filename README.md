# README #

Time Series Forecasting in Python

### What is this repository for? ###

* Time Series Forecasting in Python (jupyter notebook)
* Methodology is based on the following source: http://mariofilho.com/how-to-predict-multiple-time-series-with-scikit-learn-with-sales-forecasting-example
* 1.0

### Covering ... ###

* Feature Engineering
* Random Forests, Gradient Boosted Trees, XgBoost
* Prediction plots

### Dataset ###

* Input a dataset 

### Who do I talk to? ###

Konstantinos Kousias - kostas@simula.no